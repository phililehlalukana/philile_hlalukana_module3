import 'package:flutter/material.dart';
import 'package:untitled/register.dart';
import 'package:untitled/user_profile_edit.dart';
import 'package:untitled/welcome.dart';
import 'dashboard.dart';
import 'login.dart';

void main() {

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return  MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Welcome(),
        routes: {
          Welcome.routeName: (context) => const Welcome(),
          SignupScreen.routeName: (context) => SignupScreen(),
          LoginScreen.routeName: (context) => LoginScreen(),
          DashboardMain.routeName: (context) => const DashboardMain(),
          ProfilePage.routeName: (context) => const ProfilePage(),
          EditProfilePage.routeName: (context) => const EditProfilePage(),
        },
    );
  }
}
