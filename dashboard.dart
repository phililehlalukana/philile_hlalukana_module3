import 'package:flutter/material.dart';
import 'package:untitled/user_profile_edit.dart';

class Items {
  final String title;
  final String subtitle;
  
  Items({
    required this.title,
    required this.subtitle,

  }
  );
}

class DashboardMain extends StatefulWidget {
  static const routeName = '/dashboard';
  const DashboardMain({Key? key}) : super(key: key);

  @override
  DashboardMainState createState() => DashboardMainState();
}


class DashboardMainState extends State<DashboardMain> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        backgroundColor: Colors.blueGrey,
        body: Column(
          children: <Widget>[
            const SizedBox(height: 110),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(

                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const <Widget>[

                          Text("Dashboard",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,)
                          ),

                        SizedBox(height: 4),
                        Text("Home",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,)
                        ),


                      ],
                    ), 
                    const SizedBox(height: 4),
                  FloatingActionButton(
                        onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (
                  ctx) => const ProfilePage())); // Add your onPressed code here!
                  },
                        tooltip: 'View Profile',
                        child: new Icon(Icons.person),

                    ),


                  ],

                ),
              ),
            ),
            const SizedBox(height: 40),
            //TODO Grid Dashboard
            const GridDashboard()
          ],
        ),
    );

  }



}

  class GridDashboard extends StatefulWidget {

  const GridDashboard({Key? key}) : super(key: key);

  @override
  State<GridDashboard> createState() => _GridDashboardState();
}


class _GridDashboardState extends State<GridDashboard> {
   final Items qr = Items(
        title: "QR Access Control",
        subtitle: "Travelling, Check-in, Pass Out & Event Access");


   final Items tickets = Items(
      title: "Tickets Services",
      subtitle: "Purchase & Manage",
   );

    final Items rewords = Items(
      title: "Rewords",
      subtitle: "Promotions & Rewords",

    );

    final Items balance = Items(
      title: "Account",
      subtitle: "Transactions Payments & Balance",

    );

    final Items vr = Items(
      title: "Home Chillers",
      subtitle: "Subscribe" "Stream Events" "Entertainment",

    );

    final Items help = Items(
      title: "Communication",
      subtitle: "Contact Us" "Messages" "Help",

    );

    @override
    Widget build(BuildContext context) {
      List<Items> myList = [qr, tickets, rewords, balance, vr, help];
      return Flexible(
        child: GridView.count(
          childAspectRatio: 2.2,
          padding: const EdgeInsets.only(left: 16, right: 16),
          crossAxisCount: 2,
          crossAxisSpacing: 30,
          mainAxisSpacing: 30,
          children: myList.map((data) {
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                color: (
                  Colors.blue),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  const SizedBox(height: 14),
                  Text(
                    data.title,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold,)
                  ),
                  const SizedBox(height: 8),
                  Text(
                    data.subtitle,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        )
                  ),

                ],
              ),
            );
          }).toList(),
        ),
      );
    }
}
